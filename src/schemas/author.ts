import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type AuthorDocument = Author & Document;

@Schema()
export class Author {
  @Prop({ required: true, unique: true, trim: true })
  email: string;

  @Prop({ required: true, trim: true })
  name: string;

  @Prop()
  image: string;

  @Prop()
  cover: string;

  @Prop()
  work: string;

  @Prop()
  description: string;

  @Prop()
  about: string;

  @Prop({ required: true })
  reg_datetime: number;

  @Prop()
  birth_datetime: number;

  @Prop({ required: true })
  password: string;
}

export const AuthorSchema = SchemaFactory.createForClass(Author);
