import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Author } from './author';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  content: string;

  @Prop()
  cover: string;

  @Prop({ required: true })
  pub_datetime: number;

  @Prop({ type: Types.ObjectId, ref: 'Author', required: true })
  author: Author;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
