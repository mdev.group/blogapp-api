import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Param,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { CreateArticleDto } from './dto/create-article.dto';
import { ArticleService } from './article.service';
import { Article } from 'src/schemas/article';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller()
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @UseGuards(JwtAuthGuard)
  @Post('/article')
  async createArticle(
    @Request() req,
    @Body() body: CreateArticleDto,
  ): Promise<Article> {
    return await this.articleService.create({
      title: body.title,
      content: body.content,
      cover: body.cover,
      authorID: req.user.userId,
      pubdate: Number(new Date()),
    });
  }

  @Get('/article')
  async getAllArticles(): Promise<Article[]> {
    return await this.articleService.findAll();
  }

  @Get('/article/by/:id')
  async getAllByAuthor(@Param() params): Promise<Article[]> {
    return await this.articleService.findByAuthor(params.id);
  }

  @Get('/article/:id')
  async getUser(@Param() params): Promise<Article> {
    return await this.articleService.findByID(params.id);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/article/:id')
  async deleteArticleByID(@Request() req, @Param() params): Promise<Article> {
    console.log(params.id, req.user.userId);
    const res = await this.articleService.deleteByID(
      params.id,
      req.user.userId,
    );

    if (!res) {
      throw new ForbiddenException();
    }

    return res;
  }
}
