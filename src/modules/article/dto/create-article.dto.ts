import { ApiProperty } from '@nestjs/swagger';

export class CreateArticleDto {
  @ApiProperty()
  title: string;

  @ApiProperty()
  content: string;

  @ApiProperty()
  authorID: string;

  @ApiProperty()
  cover: string;

  @ApiProperty()
  pubdate: number;
}
