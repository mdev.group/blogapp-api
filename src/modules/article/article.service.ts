import { Model, FilterQuery } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateArticleDto } from './dto/create-article.dto';
import { Article, ArticleDocument } from 'src/schemas/article';

@Injectable()
export class ArticleService {
  constructor(
    @InjectModel(Article.name) private articleModel: Model<ArticleDocument>,
  ) {}

  async create(createArticleDto: CreateArticleDto): Promise<Article> {
    const created = new this.articleModel({
      title: createArticleDto.title,
      content: createArticleDto.content,
      cover: createArticleDto.cover,
      pub_datetime: createArticleDto.pubdate,
      author: createArticleDto.authorID,
    });
    return created.save();
  }

  async findAll(): Promise<Article[]> {
    return this.articleModel.find().exec();
  }

  async findByID(ID): Promise<Article> {
    return this.articleModel.findById(ID).exec();
  }

  async deleteByID(ID, userID): Promise<Article> {
    return this.articleModel
      .findOneAndDelete({
        _id: ID,
        author: userID,
      })
      .exec();
  }

  async findByAuthor(authorID): Promise<Article[]> {
    const filter: FilterQuery<ArticleDocument> = {
      author: authorID,
    };

    return await this.articleModel.find(filter).exec();
  }
}
