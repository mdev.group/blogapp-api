import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Author } from 'src/schemas/author';
import { CreateUserDto } from './dto/create-user.dto';
import { UserService } from './user.service';
import { hashSync } from 'bcrypt';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UpdateUserDto } from './dto/update-user.dto';

@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('/user')
  async createUser(@Body() body: CreateUserDto): Promise<Author> {
    const hashedPass = hashSync(body.password, 10);

    return await this.userService.create({
      email: body.email,
      name: body.name,
      password: hashedPass,
      reg_datetime: Number(new Date()),
    });
  }

  @Get('/user')
  async getAllUsers(): Promise<Author[]> {
    return await this.userService.findAll();
  }

  @Get('/user/:id')
  async getUser(@Param() params): Promise<Author> {
    return await this.userService.findByID(params.id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/user')
  update(@Request() req, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(req.user.userId, updateUserDto);
  }
}
