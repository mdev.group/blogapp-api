import { Model, FilterQuery } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Author, AuthorDocument } from 'src/schemas/author';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(Author.name) private authorModel: Model<AuthorDocument>,
  ) {}

  async create(createCatDto: CreateUserDto): Promise<Author> {
    const createdCat = new this.authorModel(createCatDto);
    return createdCat.save();
  }

  async findAll(): Promise<Author[]> {
    return this.authorModel.find().exec();
  }

  async findByID(ID): Promise<Author> {
    return this.authorModel.findById(ID).exec();
  }

  async findByEmail(email): Promise<Author> {
    const filter: FilterQuery<AuthorDocument> = {
      email,
    };

    return await this.authorModel.findOne(filter).lean().exec();
  }

  async update(ID, data): Promise<Author> {
    console.log(ID, data);
    return this.authorModel
      .findByIdAndUpdate(ID, {
        $set: {
          ...removeEmptyFields(data),
        },
      })
      .exec();
  }
}
