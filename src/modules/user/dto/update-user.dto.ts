import { ApiProperty } from '@nestjs/swagger';

export class UpdateUserDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  birth_datetime: number;

  @ApiProperty()
  image: string;

  @ApiProperty()
  cover: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  work: string;

  @ApiProperty()
  about: string;
}
